FROM balr/php7-fpm-nginx

WORKDIR /var/www/html

ADD . /var/www/html

RUN composer install
