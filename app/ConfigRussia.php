<?php

namespace app;

class ConfigRussia implements ConfigInterface
{
    private $exchangeRate;

    public function __construct($config = null)
    {
        if (!isset($config)) {
            $config = require __DIR__ . '/config.php';
        }
        $this->exchangeRate = $config['exchangeRate'];
    }

    /** @inheritdoc */
    public function getExchangeRate(): float
    {
        return $this->exchangeRate;
    }
}