<?php
namespace app;


class Calculator
{
    /** @var ConfigInterface configuration */
    private $config;

    public function __construct(ConfigInterface $config)
    {
        $this->config = $config;
    }

    public function sum(float $a, float $b)
    {
        return $a + $b;
    }

    public function sub(float $a, float $b)
    {
        return $a - $b;
    }

    public function multiply(float $a, float $b)
    {
        return $a * $b;
    }

    public function div(float $a, float $b)
    {
        return $a / $b;
    }

    public function exchange(float $dollars)
    {
        return $dollars * $this->config->getExchangeRate();
    }
}
