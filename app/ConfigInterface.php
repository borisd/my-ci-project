<?php
namespace app;


interface ConfigInterface
{
    /**
     * @return float Exchange rate to USD
     */
    public function getExchangeRate(): float;
}
