<?php
namespace tests;

use app\Calculator;
use app\ConfigRussia;
use PHPUnit\Framework\TestCase;

class CalculatorTest extends TestCase
{
    /**
     * @dataProvider sumProvider
     */
    public function testSumCorrect($a, $b, $result)
    {
        $c = new Calculator(new ConfigRussia());
        $this->assertTrue($c->sum($a, $b) === (float)$result);
    }

    /**
     * @dataProvider divProvider
     */
    public function testDivCorrect($a, $b, $result)
    {
        $c = new Calculator(new ConfigRussia());
        $this->assertTrue($c->div($a, $b) === (float)$result);
    }

    /**
     * @dataProvider exchProvider
     */
    public function testExchCorrect($a, $result)
    {
        $c = new Calculator(new ConfigRussia());
        $this->assertEquals($result, $c->exchange($a));
    }

    public function sumProvider()
    {
        return [
            [1, 2, 3],
            [1, 3, 4],
            [3, 3, 6],
            [0, 0, 0],
        ];
    }

    public function divProvider()
    {
        return [
            [1, 1, 1],
            [1, 2, 0.5],
            [4, 2, 2],
        ];
    }

    /**
     * @return array For test-server env
     */
    public function exchProvider()
    {
        return [
            [1, 3],
            [2, 6],
            [3, 9],
            [4, 12],
            [5, 15],
            [6, 18],
        ];
    }
}