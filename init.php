<?php

$allowedEnvs = ['dev', 'prod', 'test'];

if ($argc !== 2) {
    echo 'Which environment do you want the application to be initialized in? ' . implode(', ', $allowedEnvs) . PHP_EOL . PHP_EOL;
    $env = trim(fgets(STDIN));
} else {
    $env = $argv[1];
}
if (!in_array($env, $allowedEnvs)) {
    echo 'Unknown env' . PHP_EOL;
    exit(1);
}

echo $env . ' selected' . PHP_EOL;

$config = file_get_contents(__DIR__ . '/environments/' . $env . '/config.php');
$result = $config && file_put_contents(__DIR__ . '/app/config-local.php', $config);

if (!$result) {
    echo 'initializing failed' . PHP_EOL;
    exit(1);
}